﻿using Microsoft.EntityFrameworkCore;
namespace projetoBanco.Domain.Model
{
    public class BancoContext:DbContext
    {
        public BancoContext(DbContextOptions<BancoContext> options) : base(options){ }
        public DbSet<Lojista> Lojistas { get; set; }
        public DbSet<PessoaComun> Pessoas { get; set; }
        public DbSet<Transferencia> Transferencias { get; set; }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lojista>();
           
            modelBuilder.Entity<PessoaComun>();

            modelBuilder.Entity<Transferencia>();



        }

    }
}
