﻿using projetoBanco.Domain.Model;
using projetoBanco.Domain.OutModel;
using projetoBanco.Infrastructure.Data.Repository;
using projetoBanco.Infrastructure.EmailSetting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace projetoBanco.Domain.Services
{
    public class BancoServices

    //classe responsavel por implementar toda a parte de regra de negocio e autenticação 

    {
        private readonly BancoRepository _Repo;

        public BancoServices(BancoRepository repo)
        {
            _Repo = repo;
        }

        public BancoServices()
        {
        }


        /// ///////////////////////////////////////////// METODOS DE PESSOA COMUM //////////////////////////////////


        public async Task<List<PessoaComun>> Listusuario() //// retorna todos os usuarios comuns
        {

            var list = await _Repo.ListUserComun();

            return list;
        }


        public async Task<PessoaComun> ComumById(int id) // busca pessoa comum pelo id
        {
            var iten = await _Repo.GetPComumbyid(id);

            return iten;

        }

        public async Task<OutPessoaComun> Retorno(int id)
        {

            //int iten = int.Parse(id);
            var response = await ComumById(id);

            return new OutPessoaComun
            {
                Id = response.Id,
                NomeCompleto = response.NomeCompleto,
                Email = response.Email,
                Saldo = response.Saldo,


            };
        }

        //public async Task<List<OutPessoaComun>> retornoList()
        //{
        //    OutPessoaComun response = new();
        //    var retorno = await Listusuario();
        //    foreach (var v in retorno)
        //    {
        //        response = new OutPessoaComun
        //        {
        //            Id=v.Id,
        //            NomeCompleto=v.NomeCompleto,
        //            Email=v.Email
        //        };
        //    };
        //    return new OutPessoaComun(response.Id, response.NomeCompleto, response.Email);
        //}
        public async Task<PessoaComun> ComumByemail(string email) // busca pessoa comum pelo id
        {
            var iten = await _Repo.GetPComumemail(email);
            if (iten != null)
            {
                return iten;
            }
            return iten;
        }

        public async Task<PessoaComun> ComumByCpf(int Cpf) // busca pessoa comum pelo id
        {
            var iten = await _Repo.GetPCcpf(Cpf);
            if (iten != null)
            {
                return iten;
            }
            return iten;
        }

        public async Task<PessoaComun> NovoUser(PessoaComun model) ///cadastra usuario 
        {
            var ret = await _Repo.Cadastro(model);
            if (await _Repo.SaveCHangeAsync())
            {
                return ret;
            }
            return ret;
        }

        public async Task<int> AtttUserComum(PessoaComun model) /// atualiza usuario comum
        {
            var ret = await _Repo.AtualizarPComun(model);

            return ret;
        }

        public async Task<bool> DelUserPComun(int id) //deletar pessoacomum
        {
            await _Repo.DeleteUser(id);

            return true;
        }




        /// ////////////////////////////////////  METODOS DE LOJISTA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        public async Task<List<Lojista>> ListLuserLojista() //// retorna todos os usuarios lojistas
        {
            var list = await _Repo.ListLojista();

            return list;
        }

        public async Task<Lojista> LojistaByemail(string email) // busca lojista pelo email
        {
            var iten = await _Repo.GetLojistaemail(email);
            if (iten != null)
            {
                return iten;
            }
            return iten;
        }

        public async Task<Lojista> LojistaByCpf(int Cpf) // busca cpf do lojista
        {
            var iten = await _Repo.GetLojistacpf(Cpf);
            if (iten != null)
            {
                return iten;
            }
            return iten;
        }

        public async Task<Lojista> LojistaById(int id) // busca lojista pelo id do lojista
        {
            var iten = await _Repo.GetUserbyidLojista(id);
            return iten;
        }

        public async Task<Lojista> NovoUserLojista(Lojista model) //cadastra usuario lojista
        {
            var ret = await _Repo.CadastroLojista(model);
            return ret;
        }

        public async Task<int> AtttUserLojista(Lojista model) // atualiza usuario lojista
        {
            var ret = await _Repo.AtualizarLojista(model);

            return ret;
        }

        public async Task<bool> DelUserLojista(int id) // deletar lojista
        {
            await _Repo.DeleteLojista(id);

            return true;
        }



        /// ////////////////////////////////////  METODOS GERAL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        public async Task<bool> ValidaçãoEmail(string email) // validarção email e cpf no banco em ambos os bancos  
        {
            //var itemEmail = await _Repo.GetLojistaemail(email);
            //var itemEmail2 = await _Repo.GetPComumemail(email);

            if ((await _Repo.GetLojistaemail(email) != null) ||
                (await _Repo.GetPComumemail(email) != null)) /// vai verificar se existe o email no banco lojista 
            {
                return true; // tem email no banco
            }
            return false; // não tem
        }

        public async Task<bool> ValidaçãoCpf(int cpf) // validarção email e cpf no banco em ambos os bancos  
        {
            if (
                (await _Repo.GetPCcpf(cpf) != null) ||
                (await _Repo.GetLojistacpf(cpf) != null)) /// vai verificar se existe o email no banco lojista 
            {
                return true; // tem cpf no banco
            }

            return false; // não tem 

        }

        public async Task<string> AddDeposito(string email, float valor)
        {

            var PComun = await _Repo.GetPComumemail(email);
            var lojista = await _Repo.GetLojistaemail(email);
            if (PComun != null)
            {
                PComun.Saldo += valor;
                await _Repo.AtualizarPComun(PComun);
                return $"Deposito feito com sucesso para {PComun.NomeCompleto} no valor de {valor}";
            }
            else
            {
                if (lojista != null)
                {
                    lojista.Saldo += valor;
                    await _Repo.AtualizarLojista(lojista);
                    return $"Deposito feito com sucesso para {lojista.NomeCompleto} no valor de {valor} ";
                }
                else
                {
                    return "Conta não encontrada, verifique corretamente o email inserido!";
                }

            }
        }  // adiciona saldo ao usuario buscando pelo email


        public async Task<string> NovaTranferencia(Transferencia model)
        {
            var SandMail = new SandEmail();
            var userRef = await _Repo.GetPComumemail(model.EmailDe);

            if (userRef != null)
            {
                if (userRef.Email == model.EmailPara)
                {
                    return "não é possivel realizar transferencia para se mesmo";
                }
                else
                {
                    if (model.Valor <= 0)
                    {
                        return "insira um valor acima de 0";
                    }
                    else
                    {
                        if (userRef.Saldo < model.Valor)
                        {
                            return "saldo Insuficiente para realizar a transferencia";
                        }
                        else
                        {

                            var PComun = await _Repo.GetPComumemail(model.EmailPara);
                            var lojista = await _Repo.GetLojistaemail(model.EmailPara);

                            if (PComun != null)
                            {
                                SandMail.Send(PComun.Email, $"Você recebeu uma transferencia no valor de R${model.Valor}");
                                PComun.Saldo += model.Valor;
                                userRef.Saldo -= model.Valor;
                                await _Repo.AtualizarPComun(PComun);
                                await _Repo.AtualizarPComun(userRef);

                                return $"Você recebeu uma transferencia no valor de R${model.Valor} para {PComun.NomeCompleto}";
                            }
                            else
                            {
                                if (lojista != null)
                                {
                                    SandMail.Send(lojista.Email, $"Você recebeu uma transferencia no valor de R${model.Valor}");
                                    lojista.Saldo += model.Valor;
                                    userRef.Saldo -= model.Valor;
                                    await _Repo.AtualizarLojista(lojista);
                                    await _Repo.AtualizarPComun(userRef);

                                    return $"Você recebeu uma transferencia no valor de R${model.Valor} para {lojista.NomeCompleto}";
                                }
                                else
                                {
                                    return "Conta não encontrada, verifique corretamente o email inserido!";
                                }

                            }
                        }
                    }
                }

            }
            else
            {
                return $"O email ({model.EmailDe}) é de um lojista, portanto não é possivel realizar a transferencia";
            }
        }   // faz transferencia a partir do email 

    }


    /// ////////////////////////////////////  METODOS GERAL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\




}
