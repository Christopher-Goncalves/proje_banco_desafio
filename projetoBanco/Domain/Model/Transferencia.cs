﻿using System.Collections.Generic;

namespace projetoBanco.Domain.Model
{
    public class Transferencia
    {
        public int Id { get; set; }
        public string EmailDe { get; set; }
        public string EmailPara { get; set; }
        public float Valor { get; set; }
    

    }
}
