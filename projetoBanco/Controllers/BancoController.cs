﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using projetoBanco.Domain.Model;
using projetoBanco.Domain.Services;
using System.Threading.Tasks;

namespace projetoBanco.Controllers
{
    [Route("api/Banco")]
    [ApiController]
    public class BancoController : ControllerBase
    {
        private readonly BancoServices _Bservices;

        public BancoController(BancoServices services)
        {
            _Bservices = services;
        }


        [HttpGet("Model")]
        public ActionResult Get()
        {
            var trans = new Transferencia();
            return Ok(trans);
        }


        [HttpPost("Transferencia")]
        public async Task<ActionResult> Transferencia(Transferencia model)
        {
            try
            {
                var res = await _Bservices.NovaTranferencia(model);

                return Ok(res);
            }
            catch (System.Exception ex)
            {

                BadRequest("DEU RUIM" + ex);
            }
            return Ok();
        }

        [HttpPost("deposito")]
        public async Task<ActionResult> Deposito(Transferencia model)
        {
            try
            {
                var res = await _Bservices.AddDeposito(model.EmailPara, model.Valor);
                return Ok(res);
            }
            catch (System.Exception ex)
            {

                return BadRequest("=== deu ruim === " + ex);
            }
        }



    }
}
