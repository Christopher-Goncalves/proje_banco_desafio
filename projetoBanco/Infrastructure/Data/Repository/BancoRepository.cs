﻿using Microsoft.EntityFrameworkCore;
using projetoBanco.Domain.Model;
using projetoBanco.Infrastructure.EmailSetting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projetoBanco.Infrastructure.Data.Repository
{
    public class BancoRepository
    {
        //class responsavel por fazer as requisiçoes no banco de dados 

        private readonly BancoContext _Context;

        public BancoRepository(BancoContext context)
        {
            _Context = context;
        }

        public async Task<bool> SaveCHangeAsync()
        {
            return (await _Context.SaveChangesAsync()) > 0;
        }
        public void Add<t>(t entity) where t : class //adicionar entidade
        {
            _Context.Add(entity);
        }
        public void Update<t>(t entity) where t : class //atualizar endidade
        {
            _Context.Update(entity);
        }


        /// ///////////////////////////////////////////// METODOS DE PESSOA COMUM //////////////////////////////////


        public async Task<List<PessoaComun>> ListUserComun() //// retorna todos os usuarios comuns
        {
            IQueryable<PessoaComun> list = _Context.Pessoas;

            return await list.AsNoTracking().ToListAsync();
        }

        public async Task<PessoaComun> GetPComumbyid(int id)// faz a busca pelo id do usuario comun no sistema
        {
            IQueryable<PessoaComun> pessoa = _Context.Pessoas;


            return await pessoa.AsNoTracking().FirstOrDefaultAsync(h => h.Id == id);
        }


        public async Task<PessoaComun> GetPComumemail(string email)// faz a busca pelo email do usuario comun no sistema
        {
            IQueryable<PessoaComun> pessoa = _Context.Pessoas;

            return await pessoa.AsNoTracking().FirstOrDefaultAsync(h => h.Email == email);
        }


        public async Task<PessoaComun> GetPCcpf(int cpf)// faz a busca pelo email do usuario comun no sistema
        {
            IQueryable<PessoaComun> pessoa = _Context.Pessoas;

            return await pessoa.AsNoTracking().FirstOrDefaultAsync(h => h.CnpjCpf == cpf);
        }


        public async Task<PessoaComun> Cadastro(PessoaComun model) //cadastra usuario 
        {
            var SandMail = new SandEmail();
            SandMail.Send(model.Email, "Bem Vindo, cadastrado com sucesso");
            var ret = await _Context.Pessoas.AddAsync(model);
            await _Context.SaveChangesAsync();
            return (ret.Entity);
        }


        public async Task<int> AtualizarPComun(PessoaComun model) // atualiza usuario
        {
            _Context.Entry(model).State = EntityState.Modified;
            return await _Context.SaveChangesAsync();
        }


        public async Task<bool> DeleteUser(int userId) // deletar usuario
        {
            var item = await _Context.Pessoas.FindAsync(userId);//faz a busca no banco de dados pelo id que foi passado
            if (item != null)// verifica se existe o id no sistema
            {
                var SandMail = new SandEmail();
                SandMail.Send(item.Email, "sua conta foi cancelada com sucesso");
                _Context.Pessoas.Remove(item);
                await _Context.SaveChangesAsync();
                return true;
            }
            return false;

        }



        /// ////////////////////////////////////  METODOS DE LOJISTA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\




        public async Task<List<Lojista>> ListLojista() //// retorna todos os lojistas
        {
            var list = await _Context.Lojistas.ToListAsync();
            return list;
        }


        public async Task<Lojista> GetLojistaemail(string email)// faz a busca pelo email do usuario lojista no sistema
        {
            IQueryable<Lojista> lojistas = _Context.Lojistas;

            return await lojistas.AsNoTracking().FirstOrDefaultAsync(h => h.Email == email);
        }


        public async Task<Lojista> GetLojistacpf(int cpf)// faz a busca pelo email do usuario comun no sistema
        {
            IQueryable<Lojista> lojista = _Context.Lojistas;

            return await lojista.AsNoTracking().FirstOrDefaultAsync(h => h.CnpjCpf == cpf);
        }


        public async Task<Lojista> GetUserbyidLojista(int id)// faz a busca pelo id do lojista no sistema
        {
            Lojista lojista = await _Context.Lojistas.FindAsync(id);
            return lojista;
        }


        public async Task<Lojista> CadastroLojista(Lojista model) //cadastra usuario lojista
        {
            var SandMail = new SandEmail();
            SandMail.Send(model.Email, "Bem Vindo, cadastrado com sucesso");
            var res = await _Context.Lojistas.AddAsync(model);
            await _Context.SaveChangesAsync();
            return (res.Entity);
        }


        public async Task<int> AtualizarLojista(Lojista model) // atualiza usuario lojista
        {
            var SandMail = new SandEmail();
            SandMail.Send(model.Email, "Cadastro atualizado");
            _Context.Entry(model).State = EntityState.Modified;
            return await _Context.SaveChangesAsync();
        }


        public async Task<bool> DeleteLojista(int userId) // deletar usuario
        {
            var item = await _Context.Lojistas.FindAsync(userId);//faz a busca no banco de dados pelo id Lojista que foi passado
            if (item != null)// verifica se existe o id no sistema
            {
                var SandMail = new SandEmail();
                SandMail.Send(item.Email, "sua conta foi cancelada com sucesso");
                _Context.Lojistas.Remove(item);
                await _Context.SaveChangesAsync();
                return true;
            }
            return false;

        }
        /// ////////////////////////////////////  METODOS DE TRANSFERENCIA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        public async Task<Transferencia> AddTransferencia(Transferencia model) //cadastra usuario lojista
        {
            var res = await _Context.Transferencias.AddAsync(model);
            await _Context.SaveChangesAsync();
            return (res.Entity);
        }

    }
}
